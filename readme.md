# TASK MANAGER

## DEVELOPER INFO

**NAME:** Oleg Rydlev

**E-MAIL:** work.akkr@gmail.com

## SYSTEM INFO

**OS:** Linux Mint 21.2

**JDK:** Java 1.8.0_382

**RAM:** 16GB

**CPU:** i5-8250U

## BUILD PROJECT

```
mvn clean install
```

## RUN PROJECT

```
cd ./target
java -jar ./task-manager.jar
```


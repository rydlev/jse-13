package ru.t1.rydlev.tm.api.model;

import ru.t1.rydlev.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
